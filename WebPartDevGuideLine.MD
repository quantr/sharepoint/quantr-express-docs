# All WebParts must meet these requirements:
1. Load <= 5 sec, async (not blocking)
2. Must in SPFx, no more classic

# All WebParts must have these settings:

## Header
1. text
2. font family
3. font size

## Layout
1. one column
2. two column
3. three column

## Content direction
1. Vertical
2. Horizontal

## Color
1. No setting, align to sharepoint
2. Background
3. Shadow
4. Border
5. Border radius

## Data
1. List name
2. Create list based on self-define list name
3. Column 'Hidden'
4. Make sure handle 5000
5. At least one sort column

